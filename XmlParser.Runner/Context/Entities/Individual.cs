﻿using System;

namespace XmlParser.Runner.Context.Entities
{
    public class Individual
    {
        public Individual()
        {
            Id = Guid.NewGuid();
            IsValid = true;
        }

        public Guid Id { get; set; }
        public string CustomerCode { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Gender { get; set; }
        public string NationalId { get; set; }
        public string RoleOfCustomer { get; set; }
        public bool IsValid { get; set; }
        public decimal? GuaranteeAmount { get; set; }
        public string GuaranteeAmountCurrency { get; set; }

        public Guid ContractId { get; set; }
        public Contract Contract { get; set; }
    }
}
