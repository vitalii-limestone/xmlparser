﻿using System;

namespace XmlParser.Runner.Context.Entities
{
    public class ValidationError
    {
        public Guid Id { get; set; }
        public Guid RelatedEntityId { get; set; }
        public string RelatedEntityName { get; set; }
        public string Details { get; set; }
    }
}
