﻿using System;
using System.Collections.Generic;

namespace XmlParser.Runner.Context.Entities
{
    public class Contract
    {
        public Contract()
        {
            Id = Guid.NewGuid();
            IsValid = true;
        }

        public Guid Id { get; set; }
        public string ContractCode { get; set; }
        public string PhaseOfContract { get; set; }
        public decimal OriginalAmount { get; set; }
        public string OriginalAmountCurrency { get; set; }
        public decimal InstallmentAmount { get; set; }
        public string InstallmentAmountCurrency { get; set; }
        public decimal CurrentBalance { get; set; }
        public string CurrentBalanceCurrency { get; set; }
        public decimal OverdueBalance { get; set; }
        public string OverdueBalanceCurrency { get; set; }
        public DateTime DateOfLastPayment { get; set; }
        public DateTime NextPaymentDate { get; set; }
        public DateTime DateAccountOpened { get; set; }
        public DateTime RealEndDate { get; set; }
        public bool IsValid { get; set; }

        public ICollection<Individual> Individuals { get; set; }
    }
}
