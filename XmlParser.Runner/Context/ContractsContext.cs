﻿using Microsoft.EntityFrameworkCore;
using XmlParser.Runner.Context.Entities;

namespace XmlParser.Runner.Context
{
    public class ContractsContext : DbContext
    {
        private readonly string _connectionString;
        public ContractsContext(string connectionString)
        {
            _connectionString = connectionString;
        }

        public DbSet<Contract> Contracts { get; set; }
        public DbSet<Individual> Individuals { get; set; }
        public DbSet<ValidationError> Errors { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=localhost;Database=Contracts;Trusted_Connection=True;");
        }
    }
}
