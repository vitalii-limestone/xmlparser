﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using XmlParser.Runner.Context;
using XmlParser.Runner.Context.Entities;
using XmlParser.Runner.Helpers;

namespace XmlParser.Runner
{
    class Program
    {
        static void contractsSettingsValidationEventHandler(object sender, ValidationEventArgs e)
        {
            if (e.Severity == XmlSeverityType.Error)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write("ERROR: ");
                Console.WriteLine(e.Message);
                throw new Exception("Invalid xml!");
            }
        }

        static async Task Main(string[] args)
        {
            Console.WriteLine("Start parser");
            var builder = new ConfigurationBuilder().AddJsonFile("appsettings.json");
            var config = builder.Build();

            var settings = new XmlReaderSettings();
            var workingDirectoryPath = Directory.GetParent(Environment.CurrentDirectory).Parent.Parent.FullName;
            settings.Schemas.Add("http://creditinfo.com/schemas/Sample/Data", $"{workingDirectoryPath}\\Data.xsd");
            settings.ValidationType = ValidationType.Schema;
            settings.ValidationEventHandler += new ValidationEventHandler(contractsSettingsValidationEventHandler);
            settings.Async = true;

            using var context = new ContractsContext(config.GetConnectionString("DefaultConnection"));
            Console.WriteLine("Connectiong to db and applying migrations");
            context.Database.Migrate();
            Console.WriteLine("Migrations succesfully applied");

            var fileName = config.GetSection("FileName").Value;
            var filePath = $"{workingDirectoryPath}\\{fileName}";

            using (XmlReader reader = XmlReader.Create(filePath, settings))
            {
                Console.WriteLine($"Start parsing {fileName}");
                await ParseContracts(reader, context);
            }

            Console.ReadKey();
        }

        private async static Task ParseContracts(XmlReader reader, ContractsContext context)
        {
            while (reader.ReadToFollowing("Contract"))
            {
                using (XmlReader contractReader = reader.ReadSubtree())
                {
                    Contract contract = new();

                    if (contractReader.ReadToFollowing("ContractCode"))
                        contract.ContractCode = await contractReader.ReadElementContentAsStringAsync();

                    await ParseContractData(contractReader, contract);

                    List<Individual> individuals = await ParseIndividuals(contractReader, contract.Id);
                    
                    await ValidationHelper.ValidateContract(context, contract, individuals);
                    await context.Contracts.AddAsync(contract);
                    await context.Individuals.AddRangeAsync(individuals);
                    await context.SaveChangesAsync();

                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine($"Contract succesfully parsed. Contract code = {contract.ContractCode}, with {individuals.Count} individuals included");
                }
            }
        }

        private async static Task ParseSubjectRole(XmlReader reader, List<Individual> individuals)
        {
            if (reader.ReadToFollowing("CustomerCode"))
            {
                var code = await reader.ReadElementContentAsStringAsync();
                var individual = individuals.FirstOrDefault(i => i.CustomerCode == code);
                if (reader.ReadToFollowing("RoleOfCustomer"))
                {
                    individual.RoleOfCustomer = await reader.ReadElementContentAsStringAsync();
                }
                if (reader.ReadToFollowing("GuaranteeAmount"))
                {
                    ReadAmountFromSubNode(reader, out decimal amount, out string currency);
                    individual.GuaranteeAmount = amount;
                    individual.GuaranteeAmountCurrency = currency;
                }
            }
        }

        private async static Task<List<Individual>> ParseIndividuals(XmlReader reader, Guid contractId)
        {
            List<Individual> individuals = new();
            while (reader.Read())
            {
                Individual individual = new() { ContractId = contractId };
                if (reader.Name == "Individual" && reader.NodeType == XmlNodeType.Element)
                {
                    await ParseIndividual(reader.ReadSubtree(), individual);
                    individuals.Add(individual);
                }

                if (reader.Name == "SubjectRole" && reader.NodeType == XmlNodeType.Element)
                {
                    await ParseSubjectRole(reader.ReadSubtree(), individuals);
                }
            }
            
            return individuals;
        }

        private async static Task ParseIndividual(XmlReader reader, Individual individual)
        {
            if (reader.ReadToFollowing("CustomerCode"))
                individual.CustomerCode = await reader.ReadElementContentAsStringAsync();

            if (reader.ReadToFollowing("FirstName"))
                individual.FirstName = await reader.ReadElementContentAsStringAsync();

            if (reader.ReadToFollowing("LastName"))
                individual.LastName = await reader.ReadElementContentAsStringAsync();

            if (reader.ReadToFollowing("Gender"))
                individual.Gender = await reader.ReadElementContentAsStringAsync();

            if (reader.ReadToFollowing("DateOfBirth"))
                individual.DateOfBirth = DateTime.Parse(await reader.ReadElementContentAsStringAsync());

            if (reader.ReadToFollowing("NationalID"))
                individual.NationalId = await reader.ReadElementContentAsStringAsync();
        }

        private async static Task ParseContractData(XmlReader reader, Contract contract)
        {
            reader.ReadToFollowing("ContractData");
            using (XmlReader contractDataReader = reader.ReadSubtree())
            {
                contractDataReader.ReadToFollowing("PhaseOfContract");
                contract.PhaseOfContract = await contractDataReader.ReadElementContentAsStringAsync();

                if (contractDataReader.ReadToFollowing("OriginalAmount"))
                {
                    ReadAmountFromSubNode(contractDataReader, out decimal amount, out string currency);
                    contract.OriginalAmount = amount;
                    contract.OriginalAmountCurrency = currency;
                }

                if (contractDataReader.ReadToFollowing("InstallmentAmount"))
                {
                    ReadAmountFromSubNode(contractDataReader, out decimal amount, out string currency);
                    contract.InstallmentAmount = amount;
                    contract.InstallmentAmountCurrency = currency;
                }

                if (contractDataReader.ReadToFollowing("CurrentBalance"))
                {
                    ReadAmountFromSubNode(contractDataReader, out decimal amount, out string currency);
                    contract.CurrentBalance = amount;
                    contract.CurrentBalanceCurrency = currency;
                }

                if (contractDataReader.ReadToFollowing("OverdueBalance"))
                {
                    ReadAmountFromSubNode(contractDataReader, out decimal amount, out string currency);
                    contract.OverdueBalance = amount;
                    contract.OverdueBalanceCurrency = currency;
                }

                if (contractDataReader.ReadToFollowing("DateOfLastPayment"))
                    contract.DateOfLastPayment = contractDataReader.ReadElementContentAsDateTime();

                if (contractDataReader.ReadToFollowing("NextPaymentDate"))
                    contract.NextPaymentDate = contractDataReader.ReadElementContentAsDateTime();

                if (contractDataReader.ReadToFollowing("DateAccountOpened"))
                    contract.DateAccountOpened = contractDataReader.ReadElementContentAsDateTime();

                if (contractDataReader.ReadToFollowing("RealEndDate"))
                    contract.RealEndDate = contractDataReader.ReadElementContentAsDateTime();
            }
        }

        private static void ReadAmountFromSubNode(XmlReader reader, out decimal amount, out string currency)
        {
            using XmlReader amountReader = reader.ReadSubtree();
            amountReader.ReadToFollowing("Value");
            amount = reader.ReadElementContentAsDecimal();

            amountReader.ReadToFollowing("Currency");
            currency = reader.ReadElementContentAsString();
        }
    }
}
