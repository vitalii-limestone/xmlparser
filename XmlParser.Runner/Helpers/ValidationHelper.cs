﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using XmlParser.Runner.Context;
using XmlParser.Runner.Context.Entities;

namespace XmlParser.Runner.Helpers
{
    static class ValidationHelper
    {
        public async static Task ValidateContract(ContractsContext context, Contract contract, List<Individual> individuals)
        {
            if (contract.DateOfLastPayment >= contract.NextPaymentDate)
            {
                await AddError(context, contract.Id, nameof(Contract), "DateOfLastPayment attribute value must be before (in time) NextPaymentDate");
                contract.IsValid = false;
            }
            if (contract.DateAccountOpened >= contract.DateOfLastPayment)
            {
                await AddError(context, contract.Id, nameof(Contract), "DateAccountOpened attribute value must be before (in time) DateOfLastPayment");
                contract.IsValid = false;
            }
            if (individuals.Sum(i => i.GuaranteeAmount ?? 0) > contract.OriginalAmount)
            {
                await AddError(context, contract.Id, nameof(Contract), "Sum of GuaranteeAmount values must be lower than the OriginalAmount");
                contract.IsValid = false;
            }

            foreach (var individual in individuals)
                await ValidateIndividual(context, individual);
        }

        private async static Task ValidateIndividual(ContractsContext context, Individual individual)
        {
            var today = DateTime.Today;
            var age = today.Year - individual.DateOfBirth.Year;
            if (individual.DateOfBirth.Date > today.AddYears(-age)) age--;

            if (age < 18 || age > 99)
            {
                await AddError(context, individual.Id, nameof(Individual), $"Individual age = {age}, which doesn't match validation rule");
                individual.IsValid = false;
            }
        }

        private async static Task AddError(ContractsContext context, Guid entityId, string entityName, string details)
        {
            await context.Errors.AddAsync(new ValidationError
            {
                RelatedEntityId = entityId,
                RelatedEntityName = entityName,
                Details = details
            });
        }
    }
}
